module test_summation
  use funit
  implicit none

  real, parameter :: tolerance = 0.0

  !> Helper type to hold a set of data and expected sum result
  type sum_set_and_result
     real, dimension(:), allocatable :: set
     real :: expected
  end type sum_set_and_result

  ! Cases that any summation approach should get exactly
  type(sum_set_and_result), dimension(:), allocatable :: easy_cases

  ! Cases that require careful treatment of roundoff
  type(sum_set_and_result), dimension(:), allocatable :: medium_cases

  ! Cases that require careful treatment of roundoff and scale
  type(sum_set_and_result), dimension(:), allocatable :: hard_cases


contains

  @before
  subroutine setup
    implicit none
    easy_cases = [ &
         ! Simple sanity check
         sum_set_and_result(set = [0.0, 1.0, 2.0, 3.0, 4.0], expected = 10.0 ), &
         ! Simple roundoff test
         sum_set_and_result(set = [1.0, epsilon(1.0), -epsilon(1.0)], expected = 1.0 ) &
         ]

    medium_cases = [ &
         ! Harder roundoff test
         sum_set_and_result(set = [epsilon(1.0)/2.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, &
         -epsilon(1.0)/2.0], expected = 1.0 ) &
         ]

    hard_cases = [ &
         ! Challenging scale
         sum_set_and_result(set = [1.0/epsilon(1.0), 1.0, -1.0, -1.0/epsilon(1.0)], expected = 0.0 ), &
         ! Challenging scale
         sum_set_and_result(set = [1.0/epsilon(1.0), 1.0, 1.0, -1.0/epsilon(1.0)], expected = 2.0 ) &
         ]
  end subroutine setup

  @after
  subroutine teardown
    implicit none
  end subroutine teardown

  @test
  subroutine test_kahan_sum
    use summation, only: kahan_sum
    implicit none
    integer :: i
    real :: sum_result
    character(len=32) :: case_string
    character(len=*), parameter :: name = 'kahan'
    real, parameter :: easy_tolerance = 0.0
    real, parameter :: medium_tolerance = 0.0
    real, parameter :: hard_tolerance = 0.0

    do i = 1, size(easy_cases)
       sum_result = kahan_sum(easy_cases(i)%set)
       write(case_string,'("Easy case ",I0)') i
       @assertEqual(easy_cases(i)%expected, sum_result, tolerance = easy_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(medium_cases)
       sum_result = kahan_sum(medium_cases(i)%set)
       write(case_string,'("Medium case ",I0)') i
       @assertEqual(medium_cases(i)%expected, sum_result, tolerance = medium_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(hard_cases)
       sum_result = kahan_sum(hard_cases(i)%set)
       write(case_string,'("hard case ",I0)') i
       @assertEqual(hard_cases(i)%expected, sum_result, tolerance = hard_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

  end subroutine test_kahan_sum

    @test
  subroutine test_neumaier_sum
    use summation, only: neumaier_sum
    implicit none
    integer :: i
    real :: sum_result
    character(len=32) :: case_string
    character(len=*), parameter :: name = 'neumaier'
    real, parameter :: easy_tolerance = 0.0
    real, parameter :: medium_tolerance = 0.0
    real, parameter :: hard_tolerance = 0.0

    do i = 1, size(easy_cases)
       sum_result = neumaier_sum(easy_cases(i)%set)
       write(case_string,'("Easy case ",I0)') i
       @assertEqual(easy_cases(i)%expected, sum_result, tolerance = easy_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(medium_cases)
       sum_result = neumaier_sum(medium_cases(i)%set)
       write(case_string,'("Medium case ",I0)') i
       @assertEqual(medium_cases(i)%expected, sum_result, tolerance = medium_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(hard_cases)
       sum_result = neumaier_sum(hard_cases(i)%set)
       write(case_string,'("hard case ",I0)') i
       @assertEqual(hard_cases(i)%expected, sum_result, tolerance = hard_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

  end subroutine test_neumaier_sum

  @test
  subroutine test_klein_sum
    use summation, only: klein_sum
    implicit none
    integer :: i
    real :: sum_result
    character(len=32) :: case_string
    character(len=*), parameter :: name = 'klein'
    real, parameter :: easy_tolerance = 0.0
    real, parameter :: medium_tolerance = 0.0
    real, parameter :: hard_tolerance = 0.0

    do i = 1, size(easy_cases)
       sum_result = klein_sum(easy_cases(i)%set)
       write(case_string,'("Easy case ",I0)') i
       @assertEqual(easy_cases(i)%expected, sum_result, tolerance = easy_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(medium_cases)
       sum_result = klein_sum(medium_cases(i)%set)
       write(case_string,'("Medium case ",I0)') i
       @assertEqual(medium_cases(i)%expected, sum_result, tolerance = medium_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(hard_cases)
       sum_result = klein_sum(hard_cases(i)%set)
       write(case_string,'("hard case ",I0)') i
       @assertEqual(hard_cases(i)%expected, sum_result, tolerance = hard_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

  end subroutine test_klein_sum

  @test
  subroutine test_pairwise_sum
    !< NOTE: For size(set) <= 5 pairwise should be identical to regular sum
    use summation, only: pairwise_sum
    implicit none
    integer :: i
    real :: sum_result
    character(len=32) :: case_string
    character(len=*), parameter :: name = 'pairwise'
    real, parameter :: easy_tolerance = 0.0
    real, parameter :: medium_tolerance = 0.0
    real, parameter :: hard_tolerance = 2.0 !Basically pointless

    do i = 1, size(easy_cases)
       sum_result = pairwise_sum(easy_cases(i)%set)
       write(case_string,'("Easy case ",I0)') i
       @assertEqual(easy_cases(i)%expected, sum_result, tolerance = easy_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(medium_cases)
       sum_result = pairwise_sum(medium_cases(i)%set)
       write(case_string,'("Medium case ",I0)') i
       @assertEqual(medium_cases(i)%expected, sum_result, tolerance = medium_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(hard_cases)
       sum_result = pairwise_sum(hard_cases(i)%set)
       write(case_string,'("hard case ",I0)') i
       @assertEqual(hard_cases(i)%expected, sum_result, tolerance = hard_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

  end subroutine test_pairwise_sum

  @test
  subroutine test_regular_sum
    implicit none
    integer :: i
    real :: sum_result
    character(len=32) :: case_string
    character(len=*), parameter :: name = 'regular'
    real, parameter :: easy_tolerance = 0.0
    real, parameter :: medium_tolerance = epsilon(1.0)*10
    real, parameter :: hard_tolerance = 2.0  !Basically pointless

    do i = 1, size(easy_cases)
       sum_result = sum(easy_cases(i)%set)
       write(case_string,'("Easy case ",I0)') i
       @assertEqual(easy_cases(i)%expected, sum_result, tolerance = easy_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(medium_cases)
       sum_result = sum(medium_cases(i)%set)
       write(case_string,'("Medium case ",I0)') i
       @assertEqual(medium_cases(i)%expected, sum_result, tolerance = medium_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

    do i = 1, size(hard_cases)
       sum_result = sum(hard_cases(i)%set)
       write(case_string,'("hard case ",I0)') i
       @assertEqual(hard_cases(i)%expected, sum_result, tolerance = hard_tolerance, message = name // " " // trim(adjustl(case_string)))
    end do

  end subroutine test_regular_sum

end module test_summation
