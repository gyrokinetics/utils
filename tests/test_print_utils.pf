module test_print_utils
  use iso_fortran_env, only: int8, int16, int32, int64, real32, real64, real128
  use funit
  implicit none
  character(len=*), parameter :: temp_filename = "some_file_name"
contains
  @before
  subroutine setup
  end subroutine setup

  @after
  subroutine teardown
    use file_utils, only: finish_file_utils
    call finish_file_utils()
    call delete_temp_file()
  end subroutine teardown

  @test
  subroutine test_write_text
    use print_utils, only: write_text
    integer :: file_unit
    character(len=*), parameter :: expected_text = "this is a warning"
    character(len=200) :: read_text

    open(newunit=file_unit, file=temp_filename, status="new", action="write")
    call write_text(expected_text, file_unit)
    close(file_unit)

    open(newunit=file_unit, file=temp_filename, status="old", action="read")
    read(file_unit, '(a)') read_text
    close(file_unit)

    @assertEqual(trim(read_text), expected_text)
  end subroutine test_write_text

  @test
  subroutine test_warning
    use print_utils, only: warning
    integer :: file_unit
    character(len=*), parameter :: expected_text = "this is a warning"
    character(len=200) :: header, read_text, footer

    open(newunit=file_unit, file=temp_filename, status="new", action="write")
    call warning(expected_text, file_unit)
    close(file_unit)

    open(newunit=file_unit, file=temp_filename, status="old", action="read")
    read(file_unit, '(a)') header
    read(file_unit, '(a)') read_text
    read(file_unit, '(a)') footer
    close(file_unit)

    @assertEqual(header(1:4), "####")
    @assertEqual(trim(read_text), expected_text)
    @assertEqual(footer(1:4), "####")
  end subroutine test_warning

  @test
  subroutine test_to_string_int8
    use print_utils, only: to_string
    integer(int8) :: tmp
    tmp = 1
    @assertEqual('1', to_string(tmp))
  end subroutine test_to_string_int8

  @test
  subroutine test_to_string_int16
    use print_utils, only: to_string
    integer(int16) :: tmp
    tmp = 1
    @assertEqual('1', to_string(tmp))
  end subroutine test_to_string_int16

  @test
  subroutine test_to_string_int32
    use print_utils, only: to_string
    integer(int32) :: tmp
    tmp = 1
    @assertEqual('1', to_string(tmp))
  end subroutine test_to_string_int32

  @test
  subroutine test_to_string_int64
    use print_utils, only: to_string
    integer(int64) :: tmp
    tmp = 1
    @assertEqual('1', to_string(tmp))
  end subroutine test_to_string_int64

  @test
  subroutine test_to_string_real32
    use print_utils, only: to_string
    real(real32) :: expected, answer
    character(len=:), allocatable :: string
    expected = 1
    string = to_string(expected)
    read(string, *) answer
    @assertEqual(expected, answer)
  end subroutine test_to_string_real32

  @test
  subroutine test_to_string_real64
    use print_utils, only: to_string
    real(real64) :: expected, answer
    character(len=:), allocatable :: string
    expected = 1
    string = to_string(expected)
    read(string, *) answer
    @assertEqual(expected, answer)
  end subroutine test_to_string_real64

  @test
  subroutine test_to_string_real128
    use print_utils, only: to_string
    real(real128) :: expected, answer
    character(len=:), allocatable :: string
    expected = 1
    string = to_string(expected)
    read(string, *) answer
    @assertEqual(expected, answer)
  end subroutine test_to_string_real128

  @test
  subroutine test_to_string_complex32
    use print_utils, only: to_string
    complex(real32) :: expected, answer
    character(len=:), allocatable :: string
    expected = 1
    string = to_string(expected)
    read(string, *) answer
    @assertEqual(expected, answer)
  end subroutine test_to_string_complex32

  @test
  subroutine test_to_string_complex64
    use print_utils, only: to_string
    complex(real64) :: expected, answer
    character(len=:), allocatable :: string
    expected = 1
    string = to_string(expected)
    read(string, *) answer
    @assertEqual(expected, answer)
  end subroutine test_to_string_complex64

  @test
  subroutine test_to_string_complex128
    use print_utils, only: to_string
    complex(real128) :: expected, answer
    character(len=:), allocatable :: string
    expected = 1
    string = to_string(expected)
    read(string, *) answer
    @assertEqual(expected, answer)
  end subroutine test_to_string_complex128

  @test
  subroutine test_to_string_logical
    use print_utils, only: to_string
    logical :: expected, answer
    character(len=:), allocatable :: string
    expected = .true.
    string = to_string(expected)
    read(string, *) answer
    @assertEqual(expected, answer)

    expected = .false.
    string = to_string(expected)
    read(string, *) answer
    @assertEqual(expected, answer)
  end subroutine test_to_string_logical

  subroutine delete_temp_file
    integer :: file_unit, stat
    open(newunit=file_unit, file=temp_filename, status="old", iostat=stat)
    if (stat == 0) close(file_unit, status="delete")
  end subroutine delete_temp_file
end module test_print_utils
