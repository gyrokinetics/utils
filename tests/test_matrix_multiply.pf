module test_matrix_multiply
  use funit
  use matrix_multiply

  implicit none

  real, parameter :: tolerance = epsilon(1.0)*1.0e4

  @testParameter(constructor=newMyParam)
  type, extends (AbstractTestParameter) :: MyParamType
     integer :: npts
     type(matrix_multiply_method_type) :: method_type
   contains
     procedure :: toString
  end type MyParamType

  @testCase(constructor=newMyTestCase)
  type, extends (ParameterizedTestCase) :: MyTestCase
     type(MyParamType) :: param
   contains
     procedure :: message
     procedure :: case_name
  end type mytestcase

contains
  !----------------------------------
  ! Parameter helper routines
  !----------------------------------

  function case_name(self)
    class(MyTestCase), intent(in) :: self
    character(:), allocatable :: case_name
    integer :: lapack, intrinsic_mm, custom
    lapack = matmul_lapack%get_flag()
    intrinsic_mm = matmul_intrinsic%get_flag()
    custom = matmul_custom%get_flag()

    if (self%param%method_type%get_flag() == lapack) then
       case_name = "lapack"
    else if (self%param%method_type%get_flag() == intrinsic_mm) then
       case_name = "intrinsic"
    else if (self%param%method_type%get_flag() == custom) then
       case_name = "custom"
    else
       case_name = "UNKNOWN"
    end if
  end function case_name

  function message(self, extra)
    implicit none
    class(MyTestCase), intent(in) :: self
    character(*), intent(in) :: extra
    character(:), allocatable :: message
    character(len=6) :: npts_str
    message = extra//" case = "//self%case_name()//" "//self%param%toString()
  end function message

  function get_parameters() result(params)
    implicit none
    type(MyParamType), dimension(:), allocatable :: params
    integer, parameter, dimension(*) :: npts_vals = [1, 2, 16, 137]
    integer :: nmethods
    integer :: ipts, imethod, counter
    nmethods = size(multiply_methods)
    allocate(params(size(npts_vals)*nmethods))

    counter = 0
    do ipts = 1, size(npts_vals)
       do imethod = 1, nmethods
          counter = counter + 1
          params(counter) = newMyParam(npts = npts_vals(ipts), &
               method_type = multiply_methods(imethod))
       end do
    end do

  end function get_parameters

  function toString(this) result(string)
    class (MyParamType), intent(in) :: this
    character(:), allocatable :: string
    character(1024) :: str

    write(str,'("(npts = ",I0,")")') this%npts
    string = str
  end function toString

  function newMyParam(npts, method_type) result(param)
    type (MyParamType) :: param
    integer, intent(in) :: npts
    type(matrix_multiply_method_type), intent(in) :: method_type
    param%npts = npts
    param%method_type = method_type
  end function newMyParam

  function newMyTestCase(param)
    type (MyTestCase) :: newMyTestCase
    type (MyParamType), intent(in) :: param
    newMyTestCase%param = param
  end function newMyTestCase

  function generate_matrix(npoints) result(matrix)
    implicit none
    integer, intent(in) :: npoints
    complex, dimension(:, :), allocatable :: matrix
    real :: temp_r, temp_i
    integer :: j, k
    allocate( matrix( npoints, npoints))

    ! Initialise matrix
    do j = 1, npoints
       do k = 1, npoints
          call random_number( temp_r)
          call random_number( temp_i)
          matrix(j,k) = cmplx( temp_r, temp_i)
       end do

       ! Try to ensure matrix is diagonally dominant
       matrix(j, j) = matrix(j, j) + 10.0
    end do

  end function generate_matrix

  ! Start of tests

  @test(testParameters={get_parameters()})
  subroutine test_matrix_multiplication(this)
    implicit none
    class(MyTestCase), intent(inout) :: this
    complex, dimension(:, :), allocatable :: original_matrix_A, original_matrix_B
    complex, dimension(:, :), allocatable :: result_matrix
    original_matrix_A = generate_matrix(this%param%npts)
    original_matrix_B = generate_matrix(this%param%npts)
    result_matrix = matmul_wrapper(original_matrix_A, original_matrix_B, this%param%method_type)
    @assertEqual(0, maxval(abs(result_matrix - matmul(original_matrix_A, original_matrix_B))), tolerance = tolerance, message = this%message("Max val"))
  end subroutine test_matrix_multiplication

  @test(testParameters={MyParamType(npts=1, method_type = matmul_intrinsic)})
  subroutine test_matrix_multiply_flags_unique(this)
    implicit none
    class(MyTestCase), intent(inout) :: this
    integer, dimension(:), allocatable :: flags
    integer :: i, nmethod
    type(matrix_multiply_method_type) :: temp
    nmethod = size(multiply_methods)

    allocate(flags(nmethod))
    do i = 1, nmethod
       temp = multiply_methods(i)
       flags(i) = temp%get_flag()
    end do

    do i = 1, nmethod - 1
       @assertFalse(any(flags(i+1:) == flags(i)), message = "Checking for duplicate flags")
    end do

  end subroutine test_matrix_multiply_flags_unique

end module test_matrix_multiply
