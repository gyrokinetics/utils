#include "define.inc"
!> Provides a module around the fftw3 includes in order to generate interfaces etc.
!> We do this here instead of directly in fft_work so that in fft_work we can use
!> renaming via use to simplify call sites for different precisions
module fft_wrapper
  use, intrinsic:: iso_c_binding
#if FFT == _FFTW3_
# ifdef ASLFFTW3
#include "aslfftw3.f03"
# else
#include "fftw3.f03"
# endif
#else
  !> Constant for forward transforms, from real space to spectral space
  integer, parameter :: FFTW_FORWARD = -1
  !> Constant for inverse transforms, from spectral space to real space
  integer, parameter :: FFTW_BACKWARD = 1
#endif
end module fft_wrapper
