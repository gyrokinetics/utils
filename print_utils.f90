!> Various utilities for printing text
module print_utils
  use iso_fortran_env, only: real32, real64, real128, int8, int16, int32, int64
  implicit none
  private
  public :: write_text, warning
  public :: to_string

  interface to_string
    module procedure :: to_string_integer_int8, to_string_integer_int16
    module procedure :: to_string_integer_int32, to_string_integer_int64
    module procedure :: to_string_real_real32, to_string_real_real64, to_string_real_real128
    module procedure :: to_string_complex_real32, to_string_complex_real64, to_string_complex_real128
    module procedure :: to_string_logical
  end interface to_string

contains
  !> Print `text` to both screen and (optionally) a file
  subroutine write_text(text, file_unit)
    !> Text to write to screen and file
    character(len=*), intent(in) :: text
    !> Unit of open file to write warning to
    integer, intent(in), optional :: file_unit

    write(*, fmt='(a)') text

    if (present(file_unit)) then
      write(file_unit, fmt='(a)') text
    end if
  end subroutine write_text

  !> Print `text` in a warning box, optionally to file
  subroutine warning(text, file_unit)
    !> Text of warning
    character(len=*), intent(in) :: text
    !> Unit of open file to write warning to
    integer, intent(in), optional :: file_unit

    character(len=*), parameter :: header = "################# WARNING #######################"

    call write_text(header, file_unit)
    call write_text(text, file_unit)
    call write_text(header, file_unit)
  end subroutine warning

  !> Convert `object` to string
  pure function to_string_integer_int8(number) result(string)
    integer(int8), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "(g0)") number
    string = trim(adjustl(string))
  end function to_string_integer_int8

  pure function to_string_integer_int16(number) result(string)
    integer(int16), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "(g0)") number
    string = trim(adjustl(string))
  end function to_string_integer_int16

  pure function to_string_integer_int32(number) result(string)
    integer(int32), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "(g0)") number
    string = trim(adjustl(string))
  end function to_string_integer_int32

  pure function to_string_integer_int64(number) result(string)
    integer(int64), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "(g0)") number
    string = trim(adjustl(string))
  end function to_string_integer_int64

  pure function to_string_real_real32(number) result(string)
    real(real32), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "(g0.8)") number
    string = trim(adjustl(string))
  end function to_string_real_real32

  pure function to_string_real_real64(number) result(string)
    real(real64), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "(g0.8)") number
    string = trim(adjustl(string))
  end function to_string_real_real64

  pure function to_string_real_real128(number) result(string)
    real(real128), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "(g0.8)") number
    string = trim(adjustl(string))
  end function to_string_real_real128

  pure function to_string_complex_real32(number) result(string)
    complex(real32), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "('(', g0.8, ',', g0.8, ')')") number
    string = trim(adjustl(string))
  end function to_string_complex_real32

  pure function to_string_complex_real64(number) result(string)
    complex(real64), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "('(', g0.8, ',', g0.8, ')')") number
    string = trim(adjustl(string))
  end function to_string_complex_real64

  pure function to_string_complex_real128(number) result(string)
    complex(real128), intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "('(', g0.8, ',', g0.8, ')')") number
    string = trim(adjustl(string))
  end function to_string_complex_real128

  pure function to_string_logical(number) result(string)
    logical, intent(in) :: number
    character(len=:), allocatable :: string
    allocate(character(len=256)::string)
    write(string, "(L1)") number
    string = trim(adjustl(string))
  end function to_string_logical

end module print_utils
