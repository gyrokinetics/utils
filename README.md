Gyrokinetic utilities
=====================

[![pipeline status](https://gitlab.com/gyrokinetics/utils/badges/master/pipeline.svg)](https://gitlab.com/gyrokinetics/utils/-/commits/master)
[![Coverage Status](https://coveralls.io/repos/bitbucket/gyrokinetics/utils/badge.svg)](https://coveralls.io/bitbucket/gyrokinetics/utils)

A collection of various Fortran utilities used by some gyrokinetic codes.

Features include:

- wrappers for MPI routines so that they can be used in serial
- wrappers for some NetCDF routines that including error handling
- backports of special mathematical functions (such as `bessel`) that
  can be used with pre-F2008 compilers
- Mersenne Twister random number generator
- sorting routines

Requirements
------------

`Utils` requires a Fortran compiler that supports at least the F2008
standard. We regularly test with gfortran 11 and Intel 2022.

We recommend using at least gfortran 9.3, or Intel 19. Earlier
versions may still work but we can make no guarantees.

`Utils` has no required external dependencies, but does have the
following optional ones:

- MPI
- NetCDF (requires >= v4.2.0)
- HDF5 (only required for parallel netCDF)
- FFTW (support for v2 will be dropped in the near future)
- LAPACK
- NAG
- python (required for code generation, tests and documentation,
  requires >= 3.6)

### Fortran standard features

Some compilers still only support a subset of the F2003/F2008 standards. The
below is a non-exhaustive list of features from these standards that
we require. You can try building the [Fortran
Features](https://bitbucket.org/gyrokinetics/fortran-features/) repo
to see if your compiler supports the features we require.

#### Fortran 2003

- object oriented features, including:
    - type-bound procedures
    - runtime polymorphism, including `class(*)`
- allocatable `character`
- `import` in `interface`
- `iso_c_binding` intrinsic module
- `command_argument_count` and `get_command_argument` intrinsic
  procedures

#### Fortran 2008

- `iso_fortran_env` intrinsic module, including:
    - kind constants
    - `compiler_version` and `compiler_options`

Building
--------

Currently, this is really intended to be built as part of another
project, for example GS2. There is an experimental CMake build system,
which is only used for building and running tests as part of CI for
now.

You can use CMake to build and run the tests locally like so:

```
cmake . -B build
cmake --build build --target check
```

You may need to pass a few flags to the first `cmake` command to tell
it where to find some dependencies:

```
cmake . -B build \
  -DnetCDFFortran_ROOT=/path/to/netcdf/fortran
  -DFFTW_ROOT=/path/to/fftw
```

There are a few build options:

- `GK_ENABLE_LAPACK`: Enable LAPACK (default: on)
- `GK_ENABLE_MPI`: Enable MPI (default: on)
- `GK_ENABLE_SHMEM`: Enable SHMEM, requires MPI (default: on)
- `GK_ENABLE_FFT`: Enable FFTs (default: on)
- `GK_ENABLE_NETCDF`: Enable NetCDF (default: on)
- `GK_ENABLE_DOUBLE`: Promotes precisions of real and complex to double
  (default: on)
- `GK_ENABLE_TESTS`: Enable pFUnit-based tests (default: on)
- `GK_ENABLE_LOCAL_SPFUNC`: Enable local special functions" (default: off)
- `GK_ENABLE_NAGLIB`: Use the NAG library (default: off)
- `GK_DOWNLOAD_PFUNIT`: Download and compile pFUnit at configure time
  (default: on)
- `GK_ENABLE_F200X`: Enable use of F2003/F2008 functionality (default: on)

You can turn these on or off with `-D<option name>=ON/OFF`

Adding pFUnit tests
-------------------

New tests can be added by placing the relevant `.pf` file in the tests
directory and adding `gk_add_test( "tests/<test_file>.pf")
<test_name>)` where `test_file` is the file name of the new test and
`test_name` is a unique name for this test. For tests which follow the
pattern `<test_file>.pf = test_<test_name>.pf` then one can instead
just add `<test_name>` to the `gk_add_stanard_test` call in
CMakeLists.txt.

It is worth noting that the pFUnit setup has a few expectations about
the naming and structure of tests. In particular:

1. It is assumed that the test file contains a module with the same
name as the file (e.g. `tests/test_splines.pf` contains a module named
`test_splines`).
2. It is assumed that the first module in the file
is the one which contains tests.
