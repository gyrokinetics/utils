#!/bin/bash

set -e

header() {
    echo ""
    echo "###############################################"
    echo " "${1:-"UNNAMED SECTION"}
    echo "###############################################"
    echo ""
}

build_utils() {
    header "Build utils"
    which cmake
    cmake --version
    cmake . -B build ${EXTRA_MAKE_FLAGS} "${EXTRA_FORTRAN_FLAGS}" || (echo build_utils_failed > ${JOB_STATUS_FILE} ; exit 1)
    cmake --build build --target gkutils || (echo build_utils_failed > ${JOB_STATUS_FILE} ; exit 1)
}

make_and_run_pfunit_tests() {
    header "Build and run pfunit tests"
    cmake --build build --target check
}

if [[ "x${GK_CI_BUILD}" != "x" ]]
then
    time build_utils
fi

if [[ "x${GK_CI_PFUNIT}" != "x" ]]
then
    time make_and_run_pfunit_tests
fi
